<div id="home">

La créativité n'a plus de limites

Construis et décore avec les nouveaux ensembles LEGO

Visiter la boutique

Boutique en ligne

Ne passe pas à côté des derniers ensembles LEGO, et complète ta collection
dès maintenant en visitant la boutique en ligne. Ne manque pas les bonnes
affaires et les promotions tout au long de l'année sur certains articles !
La livraison est gratuite pour toute commande de plus de 50€.

Visiter la boutique

Réseaux sociaux

Photographie ta collection ou met en scène tes constructions au travers de
vidéos, puis partage ton expérience avec le monde entier en rejoignant la
communauté LEGO sur les réseaux sociaux.

Facebook

Instagram

YouTube

</div>